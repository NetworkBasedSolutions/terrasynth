package io.terrasynth.app;

import com.jme3.app.DebugKeysAppState;
import com.jme3.app.FlyCamAppState;
import com.jme3.app.SimpleApplication;
import com.jme3.app.StatsAppState;
import com.jme3.asset.plugins.FileLocator;
import com.jme3.audio.AudioData;
import com.jme3.audio.AudioListenerState;
import com.jme3.audio.AudioNode;
import com.jme3.audio.AudioSource;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Box;
import org.tinylog.Logger;

public class Main extends SimpleApplication {
    private static final String AUDIO_DIR = "C:\\Users\\Nick\\Music\\";
    private static final String AUDIO_FILE_NAME = "Repossession - A Synthwave Music Mix.ogg";

    private AudioNode music;

    private ActionListener musicPlayPauseAction = new ActionListener() {
        @Override
        public void onAction(String name, boolean keyPressed, float tpf) {
            if (name.equals("PlayPause") && !keyPressed) {
                if (music.getStatus() != AudioSource.Status.Playing) {
                    music.play();
                } else {
                    music.pause();
                }
            }
        }
    };

    public Main() {
        super(
            new StatsAppState(),
            new FlyCamAppState(),
            new AudioListenerState(),
            new DebugKeysAppState()
        );

        Logger.info("Terra Synth v1.0");
    }

    public static void main(String[] args) {
        Main app = new Main();
        app.start();
    }

    @Override
    public void simpleInitApp() {
        flyCam.setMoveSpeed(40);

        Box box = new Box(1, 1, 1);
        Geometry geometry = new Geometry("Box", box);
        Material material = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        material.setColor("Color", ColorRGBA.Blue);
        geometry.setMaterial(material);
        rootNode.attachChild(geometry);

        initAudio();
        initKeys();
    }

    private void initAudio() {
        assetManager.registerLocator(AUDIO_DIR, FileLocator.class);

        music = new AudioNode(
            assetManager,
            AUDIO_FILE_NAME,
            AudioData.DataType.Stream
        );

        music.setLooping(true);
        music.setPositional(false);
        music.setVolume(3);

        rootNode.attachChild(music);

        music.play();
    }

    private void initKeys() {
        inputManager.addMapping("PlayPause", new KeyTrigger(KeyInput.KEY_SPACE));
        inputManager.addListener(musicPlayPauseAction, "PlayPause");
    }
}
